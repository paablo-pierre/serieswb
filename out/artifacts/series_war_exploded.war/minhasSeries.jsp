
<%--
  Created by IntelliJ IDEA.
  User: pablo
  Date: 10/11/2018
  Time: 01:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="acesso.jsp"%>
<%@ page import="java.sql.*" %>
<%
    String user = (String) session.getAttribute("usuarioLogado");

    String sqlMinhasSeries = "SELECT DISTINCT serie.idserie, serie.descricao, serie.nome_serie, serie.qntd_temporada\n" +
            "  FROM ((serie\n" +
            "  INNER JOIN serie_has_usuario u on serie.idserie = u.serie_idserie)\n" +
            "  INNER JOIN serie_has_usuario ON u.usuario_nomeUsuario = '" + user + "') ORDER BY serie.idserie";

    ResultSet rs = banco.executeQuery(sqlMinhasSeries);

%>

<html>
<head>
    <title> Minhas Séries </title>
    <link href="resource/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="shortcut icon" href="resource/image/series1.png" type="image/x-icon"/>

    <!-- jQuery library -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>

    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</head>
<body>
    <!-- Nav bar inicia aqui -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="margin-bottom: 30%">
        <a class="navbar-brand" href="menu.jsp"> Séries Web </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="menu.jsp"> Home <span class="sr-only"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meuPerfil.jsp"> Perfil </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="minhasSeries.jsp"> Minhas séries  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="menu.jsp"> Séries </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"> <%= user %> </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="efetuarLogout.jsp"> Sair </a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"> Pesquisar </button>
            </form>
        </div>
    </nav>
    <!-- Nav bar termina aqui -->


        <table class="table" style="margin-top: 5%;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Quantidade de temporadas</th>
                    <th scope="col">Número de Episódios</th>
                    <th scope="col">Operações</th>
                </tr>
            </thead>
            <% while (rs.next()) { %>
            <tbody>
                <tr>
                    <th scope="row"> <%= rs.getInt("idserie") %> </th>
                    <td> <%= rs.getString("nome_serie") %> </td>
                    <td> <%= rs.getString("qntd_temporada") %> </td>
                    <td> 4 </td>
                    <td>

                        <a class="btn btn-danger" href="removeSerie.jsp?idserie=<%=rs.getInt("idserie")%>"
                        role="button">
                        Remover série
                        </a>
                    </td>
                </tr>
            </tbody>
            <% } %>
        </table>
</body>
</html>
