<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title> Series Web </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="resource/css/floating-label.css">
</head>
<body>
    <!-- Nav bar inicia aqui -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="margin-bottom: 30%">
        <a class="navbar-brand" href="#"> Séries Web </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href=""> Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meuPerfil.jsp"> Perfil </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="minhasSeries.jsp"> Minhas séries  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"> Séries </a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"> Pesquisar </button>
            </form>
        </div>
    </nav>
    <!-- Nav bar termina aqui -->

    <form class="form-signin" action="validaLogin.jsp" method="post">

        <div class="text-center mb-4">
            <img class="mb-4" src="./resource/image/logoSeries.png" alt="" width="180" height="180">
        </div>

        <div class="form-label-group">
            <input type="text" name="login" id="login" class="form-control" placeholder="Nome de usuário" required autofocus>
            <label for="login"> Nome de usuario</label>
        </div>

        <div class="form-label-group">
            <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required>
            <label for="senha"> Senha </label>
        </div>

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Lembrar de mim
            </label>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        <button class="btn btn-lg btn-info btn-block" onclick="window.location.href='cadastroUsuario.jsp';" type="submit"> Cadastrar </button>
    </form>


</body>
</html>
