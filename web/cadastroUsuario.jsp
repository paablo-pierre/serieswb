
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cadastro de Usuário</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <script>
        function validarSenha() {
            senha1 = document.cadastroUsuario.senha.value
            senha2 = document.cadastroUsuario.confirmar_senha.value
            if (senha1 == senha2) {
                document.cadastroUsuario.submit();
            }else {
                alert("Senhas não conferem");

            }
        }
    </script>
</head>
<body>

    <div class="col-md-8 order-md-1">
        <h4 class="mb-3"> Cadastro de Usuário </h4>
        <form class="needs-validation" action="cadUser.jsp" method="post" name="cadastroUsuario" novalidate>
            <div class="row">
                <div class="col-md-10 mb-2">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" class="form-control" id="nome" placeholder="" value="" required>
                    <div class="invalid-feedback">
                        É necessário um nome válido.
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <label for="usuario">Usuário</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">@</span>
                    </div>
                    <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Nome de usuário" required>
                    <div class="invalid-feedback" style="width: 100%;">
                        Nome de usuário é obrigatório.
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <label for="email"> Email </label>
                <input type="email" name="email" class="form-control" id="email" placeholder="email@example.com">
                <div class="invalid-feedback">
                    Email é obrigatório, por favor insira um email válido.
                </div>
            </div>

            <div class="mb-3">
                <label for="senha"> Senha </label>
                <input type="password" name="senha" class="form-control" id="senha" placeholder="Senha" required>
                <div class="invalid-feedback">
                    Por favor digite uma senha
                </div>
            </div>

            <div class="mb-3">
                <label for="confirmar_senha"> Confirmar senha </label>
                <input type="password" class="form-control" id="confirmar_senha" placeholder="Repita a senha">
            </div>

            <button class="btn btn-primary btn-lg btn-block" onclick="validarSenha();"
                    type="submit">
                Cadastrar
            </button>
        </form>
    </div>
</body>
</html>
