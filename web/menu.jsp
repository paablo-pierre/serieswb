<%-- 
    Document   : menu
    Created on : 29/10/2018, 15:14:31
    Author     : Victor Magno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="acesso.jsp"%>

<%
    String consulta = "SELECT * FROM serie ORDER BY idserie ASC";
    ResultSet res = banco.executeQuery(consulta);
%>

<!DOCTYPE html>
<html>
<head>
    <title>Séries Web</title>
    <link href="resource/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="shortcut icon" href="resource/image/series1.png" type="image/x-icon"/>


    <!-- jQuery library -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>

    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>

    <!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

</head>

<body>
    <!-- Nav bar inicia aqui -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top" style="margin-bottom: 30%">
        <a class="navbar-brand" href="menu.jsp"> Séries Web </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href=""> Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="meuPerfil.jsp"> Perfil </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="minhasSeries.jsp"> Minhas séries  </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="efetuarLogout.jsp"> Sair </a>
                </li>
                <li class="nav-item">
                   <a class="nav-link"> @<%= session.getAttribute("usuarioLogado")%> </a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"> Pesquisar </button>
            </form>
        </div>
    </nav>
    <!-- Nav bar termina aqui -->

    <!-- lista de séries inicia aqui -->
    <% while (res.next()) {
        int idserie = res.getInt("idserie");
        String nome_serie = res.getString("nome_serie");
        String qtde_temp = res.getString("qntd_temporada");
        %>
        <div class="card-deck" style="margin: 5%">
            <div class="card">
                <%--<img class="card-img-top" src=".../100px200/" alt="Card image cap">--%>
                <div class="card-body">
                    <input type="hidden" name="idserie" value="<%= res.getInt("idserie") %>"/>
                    <h5 class="card-title"> <%= res.getInt("idserie") %> - <%= res.getString("nome_serie")%> </h5>
                    <p class="card-text">  <%= res.getString("descricao")%> </p>
                    <a class="btn btn-primary"
                       href="addSerie.jsp?idserie=<%=res.getInt("idserie")%>"
                       type="submit">
                        Adicionar à minha lista
                    </a>
                    <input type="button" class="btn btn-primary" data-toggle="modal" name="modal" value="Exibir detalhes"
                           data-target="#ExemploModalCentralizado?idserie=<%=idserie%>"
                    />
                </div>
            </div>

            <!-- Início do Modal -->
            <div class="modal fade" id="ExemploModalCentralizado?idserie=<%=idserie%>" tabindex="-1"
                 role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="TituloModalCentralizado">
                                Detalhes da série <%= res.getInt("idserie") %>
                            </h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"
                                    value="<%= res.getInt("idserie") %>">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">

                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> #</th>
                                    <th scope="col"> Nome </th>
                                    <th scope="col"> Quantidade de temporadas </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <th> <%= idserie %> </th>
                                    <th> <%= nome_serie %> </th>
                                    <th> <%= qtde_temp %> </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <a class="btn btn-primary"
                               href="addSerie.jsp?idserie=<%=res.getInt("idserie")%>"
                               type="submit">
                                Adicionar à minha lista
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <% } %>
    <%--lista de séries termina aqui--%>

</body>
</html>